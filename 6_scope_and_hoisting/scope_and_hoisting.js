// scope refers to the are in which you can access a variable, variables get declared in a scope
// mainly there are 2 scopes in js
// global scope
// function scope

// global scope
// let productId = 12345;    //declared in global scope, can be accessed from anywhere in program

// if we have multiple js files linked to html file, all js files combine to give you one global scope. one js file is continuation of other js file according to the order in ehich they are linked in html file

// polluting the global scope
// occurs when you have too many variables being declared globally in the global scope, things can get very confusing iwhen variables are declared in all js files in global scope. to overcome this issue, js developers create only one variable on the global scope and we can make it a constant and that will be an onbject and every variables we have will become its properties
// const app = {
//     productId : 12345,
//     userName : 'Joe',
//     orderNumber : 789
// };





// function scope
// function showProduct(){
//     let productId = 12345;  //declared in function, can be accessed in this block only
//     console.log(productId);
// }

// showProduct();

// parameters also work like local variables





// var and hoisting
// productId = 456;
// console.log(productId); //456    //hoisting

// console.log(productId); //undefined
// var productId = 12345;

// console.log(productId); //error
// let productId = 12345;

// showProductId();            //123 //hoisting
// function showProductId(){
//     console.log(123);
// }
// here we can execute function before defining because it's a function declaration and it gets hoisted to the top. the actual mechanism behind hoisting has to do with the fact that the js file gets executed after two passes. in the first pass, the js engine will know there is a function called showProductId but in the second pass, it will actually execute things from top to down. that's why the call to showProductId is successful even though the function has not been declared yet. so hoisting is fine for functions like this but for variables and constants, use let and const and avoid var.





// undeclared variables and strict mode
// in early versions of js, you could start using variables without declaring them, and that is really a bad idea. after a few releases, js created a fix for that.
// productId = 12345;
// console.log(productId);     //12345
// here, productId is being declared on an object called window. 
// console.log(window.productId);  //12345
// window referred mainly to the window of the browser, where your app is running but many global ariables got placed in window, and with subsequent release of js, developers wanted to avoid that, they didn't want window to become a dumping ground for all kinds of variables.

// so a new feature was added to the language, where you can specify a string called 'use strict' at the top of program, it puts the js file into strict mode, and this forces you to declare your varaiables.
// 'use strict';
// productId = 12345;
// console.log(productId);     //error

// here use strict is used in global space, but you can also use it for individual functions as well.