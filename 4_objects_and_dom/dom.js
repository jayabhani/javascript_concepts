// DOM - Document Object Model

// function showMessage(text){
//     document.getElementById("message").textContent = text;
// }

// showMessage("Good Morning")

// here we have an object(document) and a method(getElementById()), which returns another object(html element), which has a property called textContent, and we set that to the parameter text.

// document is the built-in ibject refers to the web page.





// styling dom elements
// using style property and css property
// document.getElementById("message").style.color = "red";
// document.getElementById("message").style.fontWeight = "800";
// instead of - in any name, use cameCase in js





// detecting button clicks
// const but = document.getElementById("see-review");
// but.addEventListener("click", function() {
//     console.log("click done");
// });





// showing and hiding dom elements
// const but = document.getElementById("see-review");
// but.addEventListener("click", function() {
//     const rev = document.getElementById("review");
//     // rev.classList.remove("c1")
//     if(rev.classList.contains("c1")){
//         rev.classList.remove("c1");
//         but.textContent = "Close Review";
//     }else{
//         rev.classList.add("c1");
//         but.textContent = "See Review";
//     }
// });