// object properties - similar to variables attached to the objects
// object methods - similar to functions attached to the objects





// object properties
// object - group of values or properties
// let person = {
//     name : "John",
//     age : 32,
//     partTime : false
// };

// console.log(person.name);
// console.log(person.age);
// console.log(person.partTime);

// if we try to access properties which doesn't exist, it returns undefined

// person.age = 22;
// person['age'] = 22;
// console.log(person.age);





// symbol
// a symbol can be used inside of an object to hide information
// creating symbol
// let mySymbol = Symbol();

// let person = {
//     name : "John",
//     age : 32,
//     partTime : false,
//     [mySymbol] : "secretInfromation"
// };





// object methods
// let person = {
//     name : "John",
//     age : 32,
//     partTime : false,
//     showInfo : function(realAge){
//         return (this.name + " is " + realAge)
//     }
// };

// console.log(person.showInfo(25));

// console.log(typeof(person.showInfo));
// person.showInfo is a function but as it is attached to an object, it is traditionally called as method





// passing objects to functions
// let message = "hello";

// function greeting(message){
//     message = "hi";
//     console.log(message);
// }

// greeting(message);
// console.log(message);





// standard built-in objects
// Array
// Date
// Math
// String
// Number
// and so on ...
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects

// Date
// let now = new Date();
// console.log(now.toDateString());

// Math
// console.log(Math.abs(-42));
// console.log(Math.random());

// String
// let s = "hello";
// console.log(s.charAt(1));
