// creating and initializing arrays
// arrays are objects that can hold multiple values or objects.
// let values = [];  //creating an array
// let values = [1, 2, 3];     //creating and initializing an array
// let values = Array.of(1, 2, 3);     //creating and initializing an array

// console.log(values);
// console.log(typeof(values));
// console.log(Array.isArray(values));
// Array is an object and isArray() is a method attached to it





// accessing array items/elements
// let values = ['a', 'b', 'c'];
// console.log(values[0]);     //a
// console.log(values[1]);     //b
// console.log(values[3]);     //undefined
// values[0] = 'aaa';
// console.log(values);





// manipulating arrays (methods)
// push() - add element at the last position
// let values = ['a', 'b', 'c'];
// values.push('d');
// console.log(values);     //a b c d

// unshift() - adds element at the start position
// let values = ['a', 'b', 'c'];
// values.unshift('d', 'e');
// console.log(values);

// pop() - removes last element and returns that element
// let values = ['a', 'b', 'c'];
// values.pop();
// console.log(values);    //a b
// console.log(values.pop());  //b
// console.log(values);    //a

// shift() - removes first element and returns that element
// let values = ['a', 'b', 'c'];
// values.shift();
// console.log(values);    //b c
// console.log(values.shift());    //b
// console.log(values);    //c

// slice() - creates a brand new array that's based upon some slice of an original array
// let values = ['a', 'b', 'c'];
// console.log(values.slice(1, 2));    //start, end(excluded)
// console.log(values);     //b

// splice() - to insert or delete elements somewhere within the array, not just beginning or end
// let values = ['a', 'b', 'c'];
// values.splice(1, 1);    //index of elem to be deleted, no. of items to be deleted
// console.log(values.splice(1, 1));   //b
// console.log(values);    //a c


// let values = ['a', 'b', 'c'];
// values.splice(1, 0, 'foo');    //index where we want to start inserting or deleting, no. of items to be deleted, items to be inserted 
// console.log(values.splice(1, 0, 'foo'));    //[]
// console.log(values);    //a foo b c





// array searching and looping
// indexOf()
// let values = ['a', 'b', 'c'];
// console.log(values.indexOf('b'));    //1
// console.log(values.indexOf('d'));   //-1

// filter()
// let values = ['a', 'b', 'c'];
// let set = values.filter(function(item){
//     console.log(item);
//     return item > 'b';
// });
// console.log(set);   //c
// console.log(values);    //a b c

// find()
// let values = ['a', 'bbb', 'c'];
// let found = values.find(function(item){
//     return item.length > 1;
// });
// console.log(found);     //bbb
// console.log(values);    //a bbb c

// forEach()
// let values = ['a', 'b', 'c'];
// values.forEach(function(item){
//     console.log(item);
// });
// a b c





// arrays in the dom
// let containers = document.getElementsByClassName("container");
// console.log(containers);
// console.log(containers[2]);
// containers[2].classList.add('another-class');