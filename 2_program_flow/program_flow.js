// conditionals using if statement
// if(5 == 5){
//     console.log("yes");
// }





// truthy and falsy
// falsy - false, 0, "" or '' (empty strings), null, udefined, NaN
// truthy - everything NOT falsy, true, 0.5, "0" and so on..





// if ... else statement
// let n = 5;
// if(n === 5){
//     console.log("yes");
// } else {
//     console.log("no");
// }





// defference between == and ===
// == - checks only value and not data type, converts number into string
// === - checks both value and data type

// if(5 == "5"){                //true
//     console.log("yes");
// }

// if(5 === "5"){               //false
//     console.log("yes");
// }





// number precesion
// console.log(1.1 + 1.3);     //2.4000000000000004

// if(1.1 + 1.3 === 2.4){          //false
//     console.log("true");
// }

// solution is to round off the numbers
// if(+(1.1 + 1.3).toFixed(2) === 2.4){
//     console.log("true");
// }
// by putting parenthesis, it is treated as an object. we can put the dot symbol and type toFixed(2), this will create a fixed number with two decimal places. toFixed returns a string. we need to convert all of this from to a number by adding plus sign before the parenthesis.

// when you are working with floating points numbers, you always need to convert the floating point number to a fixed amount of decimal places if you are going to compare it to a literal such as 2.4

// plus sign is a simple way to convert a string returned by toFixed back into a number





// ternary operator
// (condition) ? true-statement : false-statement;
// true statement and false statement can either be values or function calls, but each is only one single statement

// let message = (5 === 5) ? "yes" : "no";
// console.log(message);

// let message;
// (5 === 5) ? message="yes" : message="no";
// console.log(message);





// block scope using let and const
// difference between let, const and var
// var - global scope
// let and const - block scope

// var and let - can be changed
// const - can't be changed

// var - if we use variable before initialization, it doesn't throw error but returns undefined
// let and const - if we use variable before initialization, it throws an error





// loops
// looping with for()
// for(let i=1; i<=5; i++){
//     console.log(i);
// }

// looping with while()
// let count = 1;
// while(count <= 5){
//     console.log(count);
//     count++;
// }

// looping with do ... while
// let count = 1;
// do{
//     console.log(count);
//     count++;
// } while(count <= 5);






// note - if we have only one line of code in a block, no need to write {}