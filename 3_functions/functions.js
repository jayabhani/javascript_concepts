// function statement or function declaration or function defination
// function logMessage(){
//     console.log("in a function");
// }

// logMessage();
// logMessage();





// function expressions
// function name is optional here, but name is recommended as it helps in debugging
// let myFn = function logMessage(){
//     console.log("in a function expression");
// }

// myFn();
// logMessage();   //error, we can't use function name to call in function expression





// passing information to function
// function logMessage(message, anotherMessage){
//     console.log(message, anotherMessage);
// }

// logMessage("hello", "world")

// note - if you don't supply values of all the parameters, they will be set to undefined.





// function return value
// function getCode(value){
//     let code = value * 5;
//     return code;
// }

// console.log(getCode(6));

// let myCode = getCode(6);
// console.log(myCode);

// note - if a function does not have a return statement, and if we call it using console.log() or by storing it into a variable, it implicitly returns undefined.





// function scope
// the parameters and local variables to that function only exist within that function and sub functions. once the function completes execution, any parameters and local variable disappear.
// function getCode(value){
//     let code = value * 5;
//     return code;
// }

// console.log(getCode(6));
// console.log(code);  //throws an error





// using function to modify web page
// function discount(percentValue){
//     document.getElementById("percent-off").textContent = percentValue + "% OFF";
// }

// discount(50);

// document is an object that refers to the whole web page, and we specify dot on the object and we call a special kind of function on that object. here we are passing getElementById() function on document object and accessing textContent of this object and we set it to parameter percentValue.