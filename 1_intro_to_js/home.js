// HTML - Structure
// CSS - Styling
// JS - Logic (Interactivity)

// JS is one of the most popular programming language, double popular than its runner up Python
// JS is a web based programming language
// For large business application, JS is popular and has been embraced by a scalable language called TypeScript. This language is a superset of JS, so we need to understand JS well in order to write large-scale business applications and other software using TypeScript.
// JS is a multi-platform language

// Applications of JS
// 1. web pages
// 2. Mac and Windows desktop apps (popular technology for it - Electron)
// 3. native smartphone and tablet apps (popular technology for it - Apache Cordova)
// 4. business apps
// 5. utility, security and data apps
// 6. games
// 7. writing server side code, we can write web servers, web APIs and other services using a technology called Node.js

// To start coding in JS, we need
// 1. Git
// 2. npm(comes bundled with Node.js)
// 3. VS Code

// To start a server
// in project directory, install dependencies using command
// npm install
// it will create "node modules" folder in project folder
// you have to run this command every time for a new project

// To run the project
// npm run start


// Ways of including JS in HTML file
// include script tag at the end of body in html
{/* <script>
    alert("Hello World");
</script> */}

// create a new file with .js extensoin and include it in html
{/* <script src="./filename.js"></script> */}
// for most web servers, it is very important to have this ./ at the beginning. that tells us the directory where the js file is located. here the dot represents the directory that the current html file is located in.







// alert("JavaScript is Amazing");
// console.log("JSJSJS");






// modifying content of page
// function showMessage(message){
//     document.getElementById('msg').textContent = message;
// }
// showMessage("This is header");






// white space -> spaces, tabs or new lines
// white spaces does not affect the output in js

// js is case sensitive

// single line comment
/*
    multi
    line
    comment
*/






// Variables

// declaring variable
// let total = 56.8;   //number
// let product = "shoes";  //string
// let validation = true;  //boolean

// let total = 56.8, product = "shoes", validation = true;

// console.log(total);

// naming variables
// start with one of _ $ letter
// followed by zero or more _ $ letter number
// can't have spaces
// can't be reserved keywords

// if the value is not assigned to the variable, it shows undefined as output.

// changing value of variable
// let price = 50;
// price = 40;
// console.log(price);







// constants
// sometimes we want a variab;e to only be set once and not change it, in that case we use constant.
// const price = 80;
// price = 87;     //throws an error
// console.log(price);






// var keyword
// with let and const - if we use the variable before initialization, it throws an error
// console.log(price);
// let price = 50;
// throws an error

// with var - if we use the variable before initialization, it doesn't throw an error but returns undefined
// console.log(price);
// var price = 50;
// this is one of the biggest problems with usingn var as a keyword
// avoid var, use let or const instead







// data types and operators
// 1. number
// let price = 20;

// arithmetic operators
// let price = 20;
// price = price + 1;
// price = price - 1;
// price = price * 1;
// price = price / 1;
// price = price % 5;
// total = price + 5;
// console.log(price);

// typeof operator
// returns data type of value or variable
// let price = 20;
// console.log(typeof(price));

// assignment operator
// let price = 20;
// price += 1;

// increment and decrement operators
// let price = 20;
// console.log(price++);   //post increment  //first returns the value and then increment it
// console.log(++price);   //pre increment  //first increment the value and then return it
// console.log(price--);   //post decrement  //first return the value and then decrement it
// console.log(--price);   //pre decrement  //first decrement the value and then return it

// operator precedence and associativity
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence

// 2. strings
// let message = "hello world";
// console.log(message);
// console.log("js is amazing");
// console.log("js is 'amazing'");
// console.log('js is "amazing"');
// console.log("js is \"amazing\"");  //escape sequence character

// template literals
// using backtick character for strings
// console.log(`year`);
// console.log(`year is ${20} and message is ${message}`);  //using interpolation

// console.log(`year is ${20} and
// message is ${message}`);

// manipulating strings
// let message = "Hello";
// message = message + " world";   //concatanation
// console.log(message);
// console.log(message.toLowerCase());
// console.log(message.toUpperCase());
// console.log(message.substring(2, 4));
// console.log(message.length);

// console.log(typeof('123'));
// console.log('123' + 4); //1234  //string  //concatanation
// 4 will also be converted into string

// converting string and number
// number to string
// generally does automatically whenever required
// let amount = 45;
// amount = amount.toString();
// console.log(typeof(amount));

// string to number
// has to be done manually always
// let amount = Number.parseFloat("756.25");
// console.log(typeof(amount));

// let amount = Number.parseFloat("A756.25");
// console.log(amount);    //NaN

// let amount = Number.parseFloat("756.25A");
// console.log(amount);    //756.25

// 3. boolean
// has 2 values : true and false
// let saved = true;
// saved = !saved;     //false
// console.log(saved);
// console.log(typeof(saved));

// 4. null and undefined
// null
// has 1 value : null
// let myNull = null;
// console.log(myNull);
// console.log(typeof(myNull));    //object, bug in js

// undefined
// has 1 value : undefined
// let myUndefined;
// myUndefined = undefined;
// console.log(myUndefined);
// console.log(typeof(myUndefined));

// difference between null and undefined is that js will set values to undefined when they are not initialized. but a programmer will set a variable to null when they want to wipe out that value.

// we can manulally set value to undefined but it's is agreed among many decelopers that when we want to wipe out the value of a variable, we use null instead of undefined.

// 5. objects and symbols
// objects
// let person = {
//     fName : "John",
//     lName : "Adams"
// };

// console.log(typeof(person));
// console.log(person);
// console.log(person.fName);
// the dot operator takes an object and pulls up one of the properties of the name, in this case fname.

// symbols
// a symbol can be used as a property in an object, such as fName, but it's hidden. there are times where you might want to hide information about an object, and you use symbols for that.
